package io.confluent.examples.clients.basicavro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class CalculatorApp {

    private static final Map<String, List<Payload>> BUFFER = new HashMap<>();
    /**
     * in millis
     */
    private static final int TIME_WINDOW = 3_000;

    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(final String[] args) {
        try (KafkaConsumer consumer = Factory.consumer();
             KafkaProducer producer = Factory.producer()
        ) {
            consumer.subscribe(Collections.singletonList(Factory.SRC_TOPIC));
            while (true) {
                final ConsumerRecords<String, Payload> records = consumer.poll(100);
                for (final ConsumerRecord<String, Payload> record : records) {
                    final Payload value = record.value();
                    final Payload stat = calculate(value);
                    final ProducerRecord<String, Payload> statRecord = new ProducerRecord<>(Factory.DST_TOPIC, stat.getId().toString(), stat);
                    producer.send(statRecord);
                }
            }
        }
    }

    private static Payload calculate(final Payload input) {

        final double statValue = getStats(input);
        System.out.printf("Statistic for msg with key %s and val %s is %s%n ", input.getId(), input.getValue(), statValue);
        input.setValue(statValue);
        return input;
    }

    private static double getStats(final Payload input) {
        final String key = String.valueOf(input.getId());
        final List<Payload> param = BUFFER.computeIfAbsent(key, k -> new ArrayList<>());
        param.add(input);
        final long currentTimeMillis = System.currentTimeMillis();
        return param.stream().filter(payload -> payload.getTs() > currentTimeMillis - TIME_WINDOW).collect(Collectors.averagingDouble(Payload::getValue));
    }
}
