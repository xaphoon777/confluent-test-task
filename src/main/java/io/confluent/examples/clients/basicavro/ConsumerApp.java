package io.confluent.examples.clients.basicavro;

import java.util.Collections;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class ConsumerApp {

    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(final String[] args) {
        try (KafkaConsumer consumer = Factory.consumer()) {
            consumer.subscribe(Collections.singletonList(Factory.DST_TOPIC));
            while (true) {
                final ConsumerRecords<String, Payload> records = consumer.poll(100);
                for (final ConsumerRecord<String, Payload> record : records) {
                    final String key = record.key();
                    final Payload value = record.value();
                    System.out.printf("key = %s, value = %s%n", key, value);
                }
            }
        }
    }
}
