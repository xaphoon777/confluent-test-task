## Описание

1. ProducerApp генерирует данные *{id,value,ts}* и отправляет в **src-topic**
2. CalculatorApp получает данные из **src-topic**, вычисляет среднее за 3 сек по каждому id и отправляет в dst-topic
3. ConsumerApp получает данные из **dst-topic** и печатает в stdout
---

## Установка

1. Развернуть и настроить платформу [Confluent: ](https://docs.confluent.io/current/quickstart/index.html)
2. Добавить топики **src-topic**, **dst-topic**: ```kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic src-topic```
3. Склонировать реп.
3. Собрать проект (```mvn clean compile package ```)
4. Запустить: ProducerApp, CalculatorApp, ConsumerApp в разных терминалах: ```mvn exec:java -Dexec.mainClass=io.confluent.examples.clients.basicavro.ProducerApp```