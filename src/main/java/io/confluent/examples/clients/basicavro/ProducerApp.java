package io.confluent.examples.clients.basicavro;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.SerializationException;

public class ProducerApp {

    public static final int METRIC_NAME = 20;
    public static final int METRIC_VALUE = 100;

    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(final String[] args) {
        try (KafkaProducer producer = Factory.producer()) {

            while (true) {
                final Payload payload = generateRandomPayload();
                final ProducerRecord<String, Payload> record = new ProducerRecord<>(Factory.SRC_TOPIC,
                        payload.getId().toString(), payload);
                producer.send(record);
                System.out.println("Produced: " + record);
                Thread.sleep(200L);
                producer.flush();
            }
        } catch (final SerializationException e) {
            e.printStackTrace();
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static Payload generateRandomPayload() {
        return new Payload((int) (Math.random() * METRIC_NAME) + "id",
                (double) ((int) (Math.random() * METRIC_VALUE)),
                System.currentTimeMillis());
    }

}

